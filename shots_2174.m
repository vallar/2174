function [shots, status] =shots_2174
% Use this script to store the array with the shots and a flag for the good
% ones
%
shots=[];
status=[]; % 1 for good shots

%% Session 1
shots_session_1 = [72746, 72747, 72748, 72749,72750, 72751, 72752, 72753, 72758, 72760,72762,72762];
status_session_1 = [1, 0, 0,0,0,1,0,0,0,0,1,0];
shots = [shots, shots_session_1];
status = [status, status_session_1];
%% Session 2
shots_session_2 = [72842, 72849, 72850, 72851, 72852, 72853, 72854];
status_session_2 = [1,0,0,1,1,1,1];
shots = [shots, shots_session_2];
status = [status, status_session_2];
%% Session 3
shots_session_3 = [72897, 72898, 72899, 72905, 72906];
status_session_3 = [1,0,1,0];
shots = [shots, shots_session_3];
status = [status, status_session_3];
end