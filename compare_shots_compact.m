%% time-traces
shots=[72746, 72751, 72761];
time_out=[[0.,2.]; [0.,2.]; [0.,2.]; [0.,2.]];
time_RZ = 0.7;
plot_delta=0;
linewidth=2.5;
leg=[];
figure('Name',sprintf('Shot #%i', shots),'Position', [10, 10, 700, 800]);
[ha, pos] = tight_subplot(5,1, 0.04, 0.1, [0.15, 0.17]);
xxticks=0.2:0.2:1.2;
ax1=ha(1); ax2=ha(2); ax3=ha(3); ax4=ha(4);ax5=ha(5);
for aa=[ax1,ax2,ax3,ax4, ax5], hold(aa, 'on'); end
% ax1=subplot(4,1,1);
col=['k', 'r', 'b', 'g'];

for ss=1:numel(shots)
  fprintf('%i',shots(ss));
  ip=gdat(shots(ss), 'ip', 'time_out', time_out(ss,1):0.001:time_out(ss,2));
%   fprintf('%.2f', mean(ip.data));
  plot(ax1, ip.t, abs(1.e-3*ip.data), col(ss), 'LineWidth', linewidth);
%   yyaxis(ax1, 'right');
  b0=gdat(shots(ss), 'b0', 'time_out', time_out(ss,1):0.001:time_out(ss,2));
  plot(ax2, b0.t, abs(b0.data), [col(ss)], 'LineWidth', linewidth);
%   yyaxis(ax1, 'left');
  nel=gdat(shots(ss), 'nel', 'time_out', time_out(ss,1):0.001:time_out(ss,2));
  plot(ax3, nel.t, nel.data*1e-19,col(ss), 'LineWidth', linewidth);
  delta=gdat(shots(ss), 'delta', 'time_out', time_out(ss,1):0.001:time_out(ss,2));
  if plot_delta
    plot(ax4, delta.t, delta.data,col(ss+1), 'LineWidth', linewidth,'DisplayName', '\delta');
  end
  deltaavg=mean(delta.data);
  zaxis=gdat(shots(ss), 'z_axis', 'time_out', time_out(ss,1):0.001:time_out(ss,2));
  plot(ax4, zaxis.t, zaxis.data,col(ss), 'LineWidth', linewidth,'DisplayName', 'z');
  pow=gdat(shots(ss), 'powers', 'time_out', time_out(ss,1):0.001:time_out(ss,2));
  if ~isempty(pow.nbi2.t)
  plot(ax5, pow.nbi2.t, pow.nbi2.data*1e-6, col(ss), 'LineWidth', linewidth, 'DisplayName', 'NB2');
  end
  if ~isempty(pow.nbi1.t)
  plot(ax5, pow.nbi1.t, pow.nbi1.data*1e-6, [col(ss),'--'], 'LineWidth', linewidth, 'DisplayName', 'NB1');
  end
  if ~isempty(pow.ec.t)
  plot(ax5, pow.ec.t, pow.ec.data(:,end)*1e-6, [col(ss), ':'], 'LineWidth', linewidth, 'DisplayName', 'ECH');
  end
  label=sprintf('#%i', shots(ss));
  leg = [leg; label];
end
ylabel(ax1, 'I_p [kA]'); xticklabels(ax1, []); xticks(ax1, xxticks);
ylabel(ax2, 'B_0 [T]'); %xticklabels(ax5, []); xticks(ax5, xxticks); ylim(ax5, [1.2, 1.5])
% ax2=subplot(4,1,2);
ylabel(ax3, 'n_{el} [10^{19} m^{-3}]');xticklabels(ax3, []); xticks(ax3, xxticks);
% ax3=subplot(4,1,3);
if ~plot_delta
  ylim(ax4);ylabel(ax4, 'z [m]');xticklabels(ax4, []); xticks(ax4, xxticks);
else
  ylim(ax4);ylabel(ax4, '');xticklabels(ax4, []); xticks(ax4, xxticks); legend(ax4, 'Location', 'Best');
end
% ax4=subplot(4,1,4);
ylabel(ax5, 'P [MW]');
xlabel('t [s]')
for a=[ax1,ax2,ax3,ax4, ax5]
  xlim(a, [0.2,2]); grid(a, 'on'); box(a, 'on');
end
title(ax1, sprintf('Shot #%i, \\delta=%.2f', shots, deltaavg));
% vline(ax1,time_analysis);
% vline(ax2,time_analysis);
% vline(ax3,time_analysis);
% vline(ax4,time_analysis);

linkaxes([ax1,ax2,ax3,ax4, ax5], 'x');
legend(ax1, leg, 'Location','best');
legend(ax5,'Location','best');

% figure;
% tcv_polview('Tvfa', shots(1), time_RZ);