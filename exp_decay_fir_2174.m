% % Script to find the exponential decay of the density on standard shots
% start_shot = 72601;
% stop_shot = 60000;
% data = find_standard_shots('start_shot', start_shot, 'stop_shot', stop_shot);

%%
data.std_shots=[72905, 72907, 72908];
t0=0.77;
t1=1.24;
% Removing shots wo exp decay
ind=data.std_shots>67680; data.std_shots = data.std_shots(ind);
data.date=[];
data.a=[]; data.b=[];
data.p1=[]; data.p2=[];
fprintf('Starting gathering data \n')
figure; hold on;
for i=1:numel(data.std_shots)
  mdsopen(data.std_shots(i));
  fir_data = mdsvalue('\results::fir:lin_int_dens[*:*:1e-3]/1e19');
  fir_time = mdsvalue('dim_of(\results::fir:lin_int_dens[*:*:1e-3]/1e19)');
  xx = mdsdata('tcv_shot_time($1)',int32(data.std_shots(i)));
  date=split(xx); date=date{1};
  if isempty(date), date=split(xx); date=date{2};end
  ind = find(fir_time<t1 & fir_time>t0);
%   figure; 
  plot(fir_time, fir_data, 'DisplayName', sprintf('%i',data.std_shots(i)));

  if ~isempty(ind)
      if data.std_shots(i)==72746
        fir_data(ind(126))=fir_data(ind(125));
        fir_data(ind(127:344))=fir_data(ind(127:344))-1.2;
      end
      ind_old=ind;
%       ind = find(fir_time<1.1 & fir_time>0.8);
      %       f = fit(fir_time(ind),fir_data(ind)-fir_data(ind(end)), 'exp1');
      f = fit(fir_time(ind),log(fir_data(ind)), 'poly1');
%       f = fit(fir_time(ind),log(fir_data(ind)), 'poly1');
%       ind=ind_old;
%       figure; plot(f, fir_time(ind),log(fir_data(ind)-fir_data(ind(end))));title(sprintf('%i',data.std_shots(i)));
%       figure; plot(fir_time(ind),fir_data(ind)-fir_data(ind(1)));title(sprintf('%i',data.std_shots(i)));
%       figure; plot(fir_time(ind),fir_data(ind));title(sprintf('%i',data.std_shots(i)));

  else
    f.a=0;
    f.b=0;
  end
%   keyboard;
try
   data.a=[data.a, f.a];
   data.b=[data.b, f.b];
catch
   data.p1=[data.p1, f.p1];
   data.p2=[data.p2, f.p2];  
end
if ~strcmp('%TDI-E-UNKNOWN_VAR,', date)
    data.date = [data.date, datetime(date)];
  else
    data.date = [data.date, datetime('2000-01-01')];
  end
  fprintf('%s on nodes, %s on data.date \n', date, datetime(date));
  end
