function [shotlist]=compare_shots_full(shotlist, f)
% Function to compare shots

set_defaults_matlab % in chease folder
debug=0;
use_gdat=0;
%% defining quantities, scales and labels
compare_quantity = {'ip', 'wmhd', 'ne_rho', 'te_rho', 'delta', 'nbi', 'b0', 'sxr'};
quantity_to_read = {'tcv_eq("i_p","liuqe.m")', 'tcv_eq("total_energy","liuqe.m")', ...
   '[t,x]=tcvget("NE0");', '[t,x]=tcvget("TE0");', ...
  'tcv_eq("delta_edge","liuqe.m")',  '\RESULTS::NBH:POWR_TCV',  'tcv_eq("BZERO","liuqe.m")', ...
   '\atlas::dt4g_mpx_001:channel_001'};
%  compare_quantity = {'ip', 'wmhd', 'delta', 'nbi', 'b0', 'sxr'};
% quantity_to_read = {'tcv_eq("i_p","liuqe.m")', 'tcv_eq("total_energy","liuqe.m")', ...
%   'tcv_eq("delta_edge","liuqe.m")',  '\RESULTS::NBH:POWR_NEUTRAL',  'tcv_eq("BZERO","liuqe.m")', ...
%    '\atlas::dt4g_mpx_001:channel_001'};
plot_scales = ones(numel(compare_quantity),1);
plot_scales(1)=1e-3; %ip
plot_scales(2)=1e-3;%wmhd
plot_scales(3) = 1e-19; %ne
plot_scales(4) = 1e-3; %ne
plot_scales(6) = 1;%nbi
plot_yaxislabel = {'I_p (kA)', 'W_{MHD} (kJ)', 'n_e(0) (10^{19} m^{-3})', 'T_e(0) (keV)' '\delta', 'P_{NBI} (MW)', 'B_0 (T)', 'SXR [AU]'};
title=['Shots ', sprintf('%i ', shotlist)];
%% defining figure
figure('Name', title, 'Position', [100, 100, 1200, 600]);
ncols=3;nrows=3; 
[axf, pos]=tight_subplot(nrows, ncols, [.07 .1],[.1 .02],[.075 .01]);
legend_label={};
ax1=axf(1);
for aa=1:numel(axf)
  hold(axf(aa),'on');
end
%% cycling on shot
for i=1:numel(shotlist)
  mdsopen(shotlist(i));
  fprintf('Shot %i \n', shotlist(i));
  for qq=1:numel(compare_quantity)
    %% fetching data
    qlabel = compare_quantity(qq); qlabel=qlabel{1};
    if strcmp(qlabel,'nbi')
      qlabel='powers';
    end
    if debug
      fprintf('q=%s\n', qlabel)
    end
    if use_gdat
      qdata = gdat(shotlist(i), qlabel, 'doplot', 0);
    else
      if ~strcmp(qlabel,'ne_rho') && ~strcmp(qlabel,'te_rho')
        fprintf('Reading %s \n', quantity_to_read{qq});
        qdata_mds=tdi(quantity_to_read{qq});
        qdata.t = qdata_mds.dim{1};
        qdata.data = qdata_mds.data;
        clear qdata_mds
      else
        fprintf('using %s \n', quantity_to_read{qq});
        eval(quantity_to_read{qq});
        qdata.t = t;
        qdata.data = x;
        clear qdata_mds
      end
    end
    fprintf('%s read \n',qlabel);
    %% plotting
    ax=axf(qq);
%     switch qlabel
%       case 'powers'
%         plot(ax, qdata.nbi.t, qdata.nbi.data*plot_scales(qq), 'Color', colos(i,:));
%       case 'ne_rho'
%         plot(ax, qdata.fit.t, qdata.fit.data(1,:)*plot_scales(qq), 'Color', colos(i,:));
%       case 'te_rho'
%         plot(ax, qdata.fit.t, qdata.fit.data(1,:)*plot_scales(qq), 'Color', colos(i,:));
%       otherwise
        try
          plot(ax, qdata.t, qdata.data.*plot_scales(qq), 'Color', colos(i,:));
        catch
          fprintf('Cannot plot \n');
        end
%     end
    clear qdata;
    box(ax, 'on'); linkaxes([ax1,ax], 'x');
    %% setting labels
    if i==1
      ylab=plot_yaxislabel(qq); ylabel(axf(qq), ylab{1});
      if qq>ncols
        xlabel(axf(qq), 't (s)');
      else
        set(axf(qq), 'XTickLabel', '');
      end
    end
  end
  mdsclose();
%   mdsdisconnect()
  legend_label{i} = num2str(shotlist(i));
end
xlim(axf(1), [0,2]);
legend(axf(1),legend_label, 'location', 'best');
legend(axf(nrows*ncols-2), legend_label, 'location', 'best');
end