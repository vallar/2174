shot_gas=72854;
shot_nbi=72852;

%% Read influx gas value
[t_gas, f_gas]=get_gas_flux(shot_gas, 1, 'D2');
f_gas = f_gas*2; %Converting it to D/s
gas_voltage = gdat(shot_gas, 'gas_request'); %Read requested voltage
%% Read NBI and FIR values
mdsopen(shot_nbi);
t_NBI=mdsvalue('dim_of(\RESULTS::NB1:CURR_NEUTRAL)');
f_NBI=mdsvalue('\RESULTS::NB1:CURR_NEUTRAL');
data_fir_nbi = mdsvalue('\results::fir:lin_int_dens[*:*:1e-3]/1e19');
time_fir_nbi = mdsvalue('dim_of(\results::fir:lin_int_dens[*:*:1e-3]/1e19)');
mdsclose();
mdsopen(shot_gas);
data_fir_gas = mdsvalue('\results::fir:lin_int_dens[*:*:1e-3]/1e19');
time_fir_gas = mdsvalue('dim_of(\results::fir:lin_int_dens[*:*:1e-3]/1e19)');
mdsclose();
%% Accounting for any given losses (e.g. duct, shine-through)
% No need to account for losses, the neutrals hitting the wall bounce off
% as cold
% You need to account for gas coming from the neutralizer. It is twice the
% current, so you need to multiply by 3
f_NBI = f_NBI*3;

%% Computing the needed voltage to match the flux from NBI
ind_NBI = t_NBI>0.855 & t_NBI<0.95;
stationary_f_NBI = mean(f_NBI(ind_NBI))./1.6e-19;
[flow_volts]=gas_flux_to_volts(stationary_f_NBI/2.);
fprintf('\n\n The required gas voltage from bottom valve the match the fuelling in shot #%i is %.3f \n\n', shot_nbi, flow_volts)
%% plot
figure('Position', [10,10,800,800]); 
ax1=subplot(3,1,1);hold on;
plot(time_fir_gas, data_fir_gas, 'DisplayName', sprintf('GAS #%i', shot_gas));
plot(time_fir_nbi,data_fir_nbi, 'DisplayName', sprintf('NBI #%i', shot_nbi));
box on; grid on;
legend show;
ylabel('FIR [1/m^{-3}]');
ax2=subplot(3,1,2);hold on;
plot(t_gas, f_gas, 'DisplayName', sprintf('GAS #%i', shot_gas));
plot(t_NBI, f_NBI./1.6e-19, 'DisplayName', sprintf('NBI #%i', shot_nbi));
xlabel('t [s]'); ylabel('Gas [#/s]');
ylim([0, 2e21]);
box on; grid on;
legend show;
ax3=subplot(3,1,3);hold on;
plot(gas_voltage.t, gas_voltage.data, 'DisplayName', sprintf('Gas #%i', shot_gas));
plot([0,2], [1,1]*flow_volts, 'DisplayName', 'Required value', 'Color', 'r');
box on; grid on;
legend show;
xlabel('t [s]'); ylabel('Request [V]');
ylim([0, 6]);
linkaxes([ax1,ax2, ax3],'x')
