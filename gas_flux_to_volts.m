function [flow_volts]=gas_flux_to_volts(input_gas_flow)
% With this function you can compute the voltage needed to have a given
% amount of gas flow.
%
% It basically inverts the equations in routine get_gas_flux.m
% It assumes valve 1, D2 gas
% 
% Inputs
% input_gas_flow(float): input of #MOLECULES/S you want. Be aware, it is
% D2/s 
%
% M. Vallar 01/2022

valve_nbr=1;
gaz_type='D2';
switch valve_nbr
  case 1
    switch gaz_type
      case 'D2'
        A_c=50.5627;
        B_c=0.4237;
        C_c=2;
        
        % 2019 calibration. factors different, curve similar:
        %A_c=37.7351;
        %B_c=0.52146;
        %C_c=2.1878;
        
      case 'He'
        A_c=72.9584;
        B_c=0.2983;
        C_c=2;

      case 'H2'   % calibrated on 7Dec 2020
        A_c=73.98;
        B_c=0.3931;
        C_c=2.048;

      case 'Ne'
        error('Valve 1 not calibrated for Ne')
      case 'N2'
        error('Valve 1 not calibrated for N2')
      otherwise
        error('Unknow gas type. The options are D2, He, Ne, and N2')
    end
  case 2
    
    if shot >=63528
      % the gains for the controller have been changed for valve 2
      % before shot 63528
      switch gaz_type
        case 'D2'
          A_c=48.5134;
          B_c=0.49178;
          C_c=2.6546;
        otherwise
          error('for shots after 63528, valve 2 is currently only calibrated for D2')
      end

    else
      switch gaz_type
        case 'D2'
          A_c=30.955;
          B_c=0.2623;
          C_c=3;
        case 'He'
          A_c=158.0017;
          B_c=0.1066;
          C_c=2.6;
        case 'Ne'
          A_c=13.9691;
          B_c=0.2695;
          C_c=2.5;
        case 'N2'
          A_c=7.9265;
          B_c=0.4535;
          C_c=2.55;
        otherwise
          error('Unknow gas type. The options are D2, He, Ne, and N2')
      end
    end
  case 3
    switch gaz_type
      case 'D2'
        disp('D2 from valve 3: It is assumed that the "fuelling gains" were used. Check logbook to be sure')
        A_c=50.9396;
        B_c=0.44955;
        C_c=2.3406;
        
        % in case we really used the "seeding gains", despite
        % injecting D2, the following would be the right
        % calibration parameters:
        %{
                A_c=27.0889;
                B_c=0.3016;
                C_c=2.8047;
        %}
      case 'He'
        A_c=72.0665;
        B_c=0.1497;
        C_c=2.4616;
      case 'Ne'
        A_c=13.1245;
        B_c=0.2847;
        C_c=2.3536;
      case 'N2'
        A_c=8.2356;
        B_c=0.4576;
        C_c=2.4;
      otherwise
        error('Unknow gas type. The options are D2, He, Ne, and N2')
    end
  otherwise
    error('The valve number needs to be 1, 2, or 3')
end

%{
Original equations
% transform Volts to mbar*l/s
flow_real=A_c*( ( (B_c*flow).^C_c+1).^(1/C_c)-1 );

% go from mbar*l/sec to molecules/sec
flow_real=flow_real*0.1/(1.38e-23*293);
%}

input_gas_flow_bar = input_gas_flow*293*1.38e-23./0.1;
flow_volts = 1./B_c*((input_gas_flow_bar./A_c+1).^C_c-1).^(1./C_c);

end